## Install

`npm install`

## Run dev. version

Build & run local server with a development version (hot reloading, CSS in JS):

`npm run start`

Build, run local server with a development version and open browser window:

`npm run open`

## Build

Build a production bundle in "/build" directory:

`npm run build`