/**
 * Check if an element is in a viewport
 * @param {Object} element
 * @param {Boolean} [partially=false]
 * @returns {Boolean}
 */
export default function is_in_view(element, partially = false) {
  const element_top = element.getBoundingClientRect().top
  const element_bottom = element.getBoundingClientRect().bottom

  let is_visible = false

  switch (partially) {
    case true:
      // Partially visible elements return true
      is_visible = element_top < window.innerHeight && element_bottom >= 0
      break;

    default:
      // Only completely visible elements return true
      is_visible = (element_top >= 0) && (element_bottom <= window.innerHeight)
      break;
  }

  // let element_height = element.getBoundingClientRect().height
  // let element_visible_height = window.innerHeight - element_top
  // let element_visible_percent = element_visible_height / element_height * 100
  //
  // if (is_visible && element_height > 900) {
  //   console.log(element_visible_percent)
  // }

  return is_visible;
}