const autoprefixer = require('autoprefixer');
const importCSS = require('postcss-import');
const clean = require('postcss-clean');

module.exports = {
  plugins: [
    importCSS(), // Needed because Stylus can't inline CSS - https://github.com/stylus/stylus/issues/2155
    autoprefixer(),
    clean({
      // format: 'beautify',
      level: 2,
      inline: false // postcss-import gets this
    })
  ]
};
